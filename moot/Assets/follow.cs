﻿
using UnityEngine;
using System.Collections;

public class follow : MonoBehaviour
{
    bool hasTarget = false;
    public float MaxDistanceToCam = 5;
    Animator MainMoot;

    void Update()
    {
        NavMeshAgent na = GetComponent<NavMeshAgent>();
       
        if (!hasTarget)
        {
            na.destination = Camera.main.transform.position;

            hasTarget = true;
        }


        if (na.velocity.magnitude < 0.5 ||
            Vector3.Distance(na.destination, Camera.main.transform.position) > MaxDistanceToCam)
            hasTarget = false;

        transform.LookAt(Camera.main.transform.position);
        transform.Rotate(0, 90, 0);
    }
}