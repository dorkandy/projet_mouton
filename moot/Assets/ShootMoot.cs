﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ShootMoot : MonoBehaviour
{

    public Rigidbody bullet;
    public float speed = 50;
    public float height = 10;
    public GameObject spawn;
    private GameObject PlayerCharacter;
    public int spawnCount = 1;

    public void Start()
    {
        PlayerCharacter = GameObject.Find("MainMoot");
    }

    public void OnTriggerEnter(Collider col)
    {
            Rigidbody instantiatedProjectile = Instantiate(bullet,
                                                               spawn.transform.position,
                                                               spawn.transform.rotation
                                                               )
                    as Rigidbody;
            instantiatedProjectile.velocity = transform.TransformDirection(new Vector3(0, height, speed));

            PlayerCharacter.GetComponent<behaveMoot>().sheepCount++;
            Debug.Log(PlayerCharacter.GetComponent<behaveMoot>().sheepCount);

            spawnCount--;

            GetComponent<AudioSource>().Play();
        if (spawnCount <= 0)
            DestroyObject(this.gameObject);

        }

    public void OnTriggerExit(Collider col)
    {

    }

}
