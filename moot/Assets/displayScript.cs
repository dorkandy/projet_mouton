﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class displayScript : MonoBehaviour {

    private GameObject PlayerCharacter;
    public int SheepCountWin = 10;

    void UpdateCount() {
        GetComponentInChildren<Text>().text = PlayerCharacter.GetComponent<behaveMoot>().sheepCount.ToString();

    }

    void DisplayWin()
    {

        GetComponentInChildren<Image>().enabled = true;
    }

    // Use this for initialization
    void Start () {
        PlayerCharacter = GameObject.Find("MainMoot");

    }
	
	// Update is called once per frame
	void Update () {
        UpdateCount();
        if (PlayerCharacter.GetComponent<behaveMoot>().sheepCount >= SheepCountWin)
            DisplayWin(); 
    
   
	
	}


}
